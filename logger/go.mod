module gitlab.com/bngnha/util/logger

go 1.14

require (
	github.com/micro/go-micro/v2 v2.3.0
	go.uber.org/zap v1.15.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.3.0
)
