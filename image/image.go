package image

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"image"
	"image/jpeg"
	"os"
	"strings"
)

// Encode2Base64 function
func Encode2Base64(img image.Image, quality int) (b64 string, err error) {
	if quality <= 0 {
		quality = 100
	}

	var b bytes.Buffer
	w := bufio.NewWriter(&b)
	if err = jpeg.Encode(w, img, &jpeg.Options{Quality: quality}); nil != err {
		return
	}

	b64 = base64.StdEncoding.EncodeToString(b.Bytes())

	return
}

// Decode2File function
func Decode2File(b64 string, baseName string) (file *os.File, err error) {
	defer file.Close()

	dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(b64))
	buf := bytes.Buffer{}
	if _, err = buf.ReadFrom(dec); err != nil {
		return
	}

	var ext string
	if _, ext, err = image.DecodeConfig(bytes.NewReader(buf.Bytes())); err != nil {
		return
	}

	file, err = os.Create(baseName + "." + ext)
	if err != nil {
		return
	}

	_, err = buf.WriteTo(file)

	return
}
