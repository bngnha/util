package workpool

import "sync"

type Job interface {
	Process()
}

type JobQueue struct {
	workers           [] *Worker
	dispatcherStopped sync.WaitGroup
	workersStopped    sync.WaitGroup
	jobRunning        chan chan Job
	internalQueue     chan Job
	quit              chan bool
}

func NewJobQueue(maxWorkers int) *JobQueue {
	workers := make([]*Worker, maxWorkers, maxWorkers)
	workersStopped := sync.WaitGroup{}
	jobRunning := make(chan chan Job, maxWorkers)

	for i := 0; i < maxWorkers; i++ {
		workers[i] = NewWorker(i, jobRunning, workersStopped)
	}

	return &JobQueue{
		internalQueue:     make(chan Job),
		jobRunning:        jobRunning,
		workers:           workers,
		dispatcherStopped: sync.WaitGroup{},
		workersStopped:    workersStopped,
		quit:              make(chan bool),
	}
}

func (jq *JobQueue) Push(job Job) {
	jq.internalQueue <- job
}

func (jq *JobQueue) Stop() {
	jq.quit <- true
	jq.dispatcherStopped.Wait()
}

func (jq *JobQueue) Start() {
	for i := 0; i < len(jq.workers); i++ {
		jq.workers[i].Run()
	}

	go func() {
		jq.dispatcherStopped.Add(1)
		for {
			select {
			case job := <-jq.internalQueue:
				go func(job Job) {
					workerChannel := <-jq.jobRunning // wait for available channel
					workerChannel <- job             // dispatch job to worker
				}(job)
			case <-jq.quit:
				for i := 0; i < len(jq.workers); i++ {
					jq.workers[i].Stop()
				}
				jq.workersStopped.Wait()
				jq.dispatcherStopped.Done()
				return
			}
		}
	}()
}