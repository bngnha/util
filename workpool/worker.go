package workpool

import (
	"sync"

	log "github.com/micro/go-micro/v2/logger"
)

type Worker struct {
	workerId         int
	done             sync.WaitGroup
	jobRunning       chan chan Job
	assignedJobQueue chan Job
	quit             chan bool
}

func NewWorker(workerID int, jobChan chan chan Job, done sync.WaitGroup) *Worker {
	return &Worker{
		workerId:         workerID,
		done:             done,
		jobRunning:       jobChan,
		assignedJobQueue: make(chan Job),
		quit:             make(chan bool),
	}
}

func (w *Worker) Run() {
	log.Info("Run worker id", w.workerId)

	go func() {
		w.done.Add(1)
		for {
			w.jobRunning <- w.assignedJobQueue
			select {
			case job := <-w.assignedJobQueue:
				log.Info("job running ", w.workerId)
				job.Process()
			case <-w.quit:
				w.done.Done()
				log.Info("stop")
				return
			}
		}
	}()
}

func (w *Worker) Stop() {
	w.quit <- true
}
