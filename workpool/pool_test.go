package workpool

import (
	"fmt"
	"testing"
)

const (
	NumberOfWorkers = 5
)

var (
	queue *JobQueue
)

func TestNewJobQueue(t *testing.T) {
	queue = NewJobQueue(NumberOfWorkers)
}

func TestJobQueue_Push(t *testing.T) {
	job := &TestJob{"1"}
	queue.Push(job)
}

func TestJobQueue_Start(t *testing.T) {
	queue.Start()
}

type TestJob struct {
	ID string
}

func (t *TestJob) Process() {
	fmt.Printf("Processing job '%s'\n", t.ID)
}

func TestJobQueue_Stop(t *testing.T) {
	queue.Stop()
}