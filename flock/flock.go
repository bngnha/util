package flock

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/gofrs/flock"
	log "github.com/micro/go-micro/v2/logger"
	// "github.com/sqweek/dialog"
)

const lockObj = ".lock"

var quit = make(chan os.Signal)

func init() {
	flocker := flock.New(lockObj)
	locked, err := flocker.TryLock()
	if err != nil {
		log.Errorf("Start exclusive program failed!: msg=[%s]", err.Error())

		return
	}

	if !locked {
		// TODO: temporarily disable because cannot build for osx
		// dialog.Message("Another instance is running!").Title("Error").Error()
		log.Error("Another instance is running!")
		os.Exit(1)
	}

	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-quit
		flocker.Close()
		os.Remove(lockObj)
		os.Exit(0)
	}()
}
