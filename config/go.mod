module gitlab.com/bngnha/util/config

go 1.14

require (
	github.com/go-openapi/inflect v0.19.0
	github.com/micro/go-micro/v2 v2.9.1
	gitlab.com/bngnha/go-plugins/logger/zap/v2 v2.0.0-20201213155100-e2f3c813bc1f
	gitlab.com/bngnha/util v0.0.0-20201213082721-01150a472af0
	gitlab.com/bngnha/util/logger v0.0.0-20201213082721-01150a472af0
)
