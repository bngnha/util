# Database utils
## Purpose
This utils is used to deal with database.
## Installation
To installation this utils, just import it into your project.
E.g:
```
import "gitlab.com/bngnha/utils/database"
```

## Usage
### To find a document
```
database.Find(${ctx}, ${mongoDB}, ${collection}, ${filter}, ${T})
```

`ctx`: Context
`mongoDB`: mongo.Database
`collection`: collection want to get
`filter`: condition, schema
`T`: inteface {}

