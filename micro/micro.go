package micro

import (
	"context"

	"github.com/micro/go-micro/v2"
	proto "github.com/micro/go-micro/v2/broker/service/proto"
	log "github.com/micro/go-micro/v2/logger"

	"gitlab.com/bngnha/util/str"
)

// Publisher struct
type Publisher struct {
	micro.Publisher
}

// PublishMsg function
func (s *Publisher) PublishMsg(message *proto.Message, logs ...string) {
	msgInfo := logs[0]
	msgError := logs[1]
	msgDebug := logs[2]
	if 0 < len(msgDebug) {
		log.Debug(msgDebug, str.Beautify(map[string]interface{}{
			"header": message.Header,
			"body":   string(message.Body),
		}))
	}
	if err := (*s).Publish(context.Background(), message); nil != err && 0 < len(msgError) {
		log.Error(msgError, str.Beautify(map[string]interface{}{
			"error_message": err.Error(),
			"error":         &err,
			"header":        message.Header,
			"body":          string(message.Body),
		}))
	} else if 0 < len(msgInfo) {
		log.Info(msgInfo)
	}
}
