package file

import "os"

// CheckOrMkdirAll function
func CheckOrMkdirAll(dirPath string) (err error) {
	if _, err = os.Stat(dirPath); os.IsNotExist(err) {
		if err = os.MkdirAll(dirPath, os.ModePerm); nil != err {
			return
		}
	}

	return
}
